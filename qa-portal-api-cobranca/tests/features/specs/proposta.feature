#language: pt

Funcionalidade: Negociar acordos pela API Portal Negociação
- Para que eu possa acessar o sistema sendo um usuário/assessoria
- Para que eu possa buscar os contratos de um CPF
- E possa simular propostas
- E possa validar se essas propostas tem probabilidade de serem aceitas
- E possa incluir as propostas simuladas anteriormente

Contexto: Pré-requisitos para autenticação no Portal Negociação
    * eu preencher o campo de usuário "02416854275"
    * o campo de senha "senha123"
    * fazer a requisição

@busca_contratos_por_cpf
Cenário: Buscando contratos por CPF
    Dado que eu faça a requisição para buscar os contratos de um CPF
    Então devo verificar se o status obtido foi '200'
    E devo verificar se a requisição de busca de contratos retornou os campos 'correctedValue', 'maxAmount' e 'id'

@simula_proposta_a_vista_valor_baixo
Cenário: Simulando proposta à vista com valor baixo
    Dado que um dos contratos listados com o campo "canNegotiate" equivalente a "true" seja selecionado
    Quando a requisição de simulação de proposta for realizada à vista com um valor baixo 
    Então devo verificar se o status obtido foi '200' com o status 'PENDING'

@inclui_acordo_a_vista_valor_baixo
Cenário: Incluindo proposta à vista com valor baixo
    Dado que um o contrato simulado anteriormente seja selecionado
    Quando a requisição de inclusão de proposta for realizada à vista com o mesmo valor baixo da simulação
    Então devo verificar se o status obtido na inclusão de acordo foi '200' com o status 'PENDING'

@simula_proposta_a_vista_valor_alto
Cenário: Simulando proposta à vista com valor alto 
    Dado que um dos contratos listados com o campo "canNegotiate" equivalente a "true" seja selecionado
    Quando a requisição de simulação de proposta for realizada à vista com um valor alto
    Então devo verificar se o status obtido foi '200' com o status 'ACCEPTED'

@executa_quebra_de_acordos
@inclui_acordo_a_vista_valor_alto
Cenário: Incluindo proposta à vista com valor alto
    Dado que um o contrato simulado anteriormente seja selecionado
    Quando a requisição de inclusão de proposta for realizada à vista com o mesmo valor alto da simulação
    Então devo verificar se o status obtido na inclusão de acordo foi '200' com o status 'ACCEPTED'

@simula_proposta_a_vista_informando_data_invalida
Cenário: Simulando proposta à vista com uma data anterior ao dia atual
    Dado que um dos contratos listados com o campo "canNegotiate" equivalente a "true" seja selecionado
    Quando a requisição de simulação de proposta for realizada à vista com uma data anterior ao dia atual
    Então devo verificar se o status code obtido foi '200' com a mensagem '-10003 - Data de pagamento menor que a data atual.'

@inclui_acordo_a_vista_informando_data_invalida
Cenário: Incluindo proposta à vista com uma data anterior ao dia atual
    Dado que um o contrato simulado anteriormente seja selecionado
    Quando a requisição de inclusão de proposta for realizada à vista com uma data anterior ao dia atual
    Então devo verificar se o status code obtido na inclusão de acordo foi '200' com a mensagem 'DATA DO VENCIMENTO DO PRIMEIRO BOLETO NAO PODE SER MENOR QUE A DATA ATUAL' 

@simula_proposta_a_vista_valor_abaixo_minimo
Cenário: Simulando proposta à vista com valor menor que dez reais
    Dado que um dos contratos listados com o campo "canNegotiate" equivalente a "true" seja selecionado
    Quando a requisição de simulação de proposta for realizada à vista com o valor de '2.00' reais
    Então devo verificar se o status obtido foi '200' com o status 'PENDING'

@inclui_acordo_a_vista_valor_abaixo_minimo
Cenário: Incluindo proposta à vista com valor menor que dez reais
    Dado que um o contrato simulado anteriormente seja selecionado
    Quando a requisição de inclusão de proposta for realizada à vista com o mesmo valor de '2.00' reais da simulação
    Então devo verificar se o status obtido na inclusão de acordo foi '200' com o status 'PENDING'

@simula_proposta_a_vista_valor_minimo
Cenário: Simulando proposta à vista com valor de dez reais
    Dado que um dos contratos listados com o campo "canNegotiate" equivalente a "true" seja selecionado
    Quando a requisição de simulação de proposta for realizada à vista com o valor de '10,00' reais
    Então devo verificar se o status obtido foi '200' com o status 'PENDING'

@inclui_acordo_a_vista_valor_minimo
Cenário: Incluindo proposta à vista com valor de dez reais
    Dado que um o contrato simulado anteriormente seja selecionado
    Quando a requisição de inclusão de proposta for realizada à vista com o mesmo valor de '10,00' reais da simulação
    Então devo verificar se o status obtido na inclusão de acordo foi '200' com o status 'PENDING'

@simula_proposta_a_vista_valor_nulo
Cenário: Simulando proposta à vista com valor nulo
    Dado que um dos contratos listados com o campo "canNegotiate" equivalente a "true" seja selecionado 
    Quando a requisição de simulação de proposta for realizada à vista com um valor nulo
    Então devo verificar se o status obtido foi '200' com o status 'PENDING'

@simula_proposta_a_vista_valor_vazio
Cenário: Simulando proposta à vista com valor vazio
    Dado que um dos contratos listados com o campo "canNegotiate" equivalente a "true" seja selecionado
    Quando a requisição de simulação de proposta for realizada à vista com um valor vazio
    Então devo verificar se o status obtido foi '500' sem mensagem

@simula_proposta_a_vista_valor_com_string
Cenário: Simulando proposta à vista com valor string
    Dado que um dos contratos listados com o campo "canNegotiate" equivalente a "true" seja selecionado
    Quando a requisição de simulação de proposta for realizada à vista com um valor string
    Então devo verificar se o status obtido foi '400' sem mensagem

@simula_proposta_a_vista_data_pagamento_nulo
Cenário: Simulando proposta à vista com a data de pagamento nula
    Dado que um dos contratos listados com o campo "canNegotiate" equivalente a "true" seja selecionado
    Quando a requisição de simulação de proposta for realizada à vista com a data de pagamento nula
    Então devo verificar se o status code obtido foi '200' com a mensagem '-10003 - Data de pagamento menor que a data atual.'

@simula_proposta_a_vista_data_pagamento_vazio
Cenário: Simulando proposta à vista com a data de pagamento vazia
    Dado que um dos contratos listados com o campo "canNegotiate" equivalente a "true" seja selecionado
    Quando a requisição de simulação de proposta for realizada à vista com a data de pagamento vazia
    Então devo verificar se o status obtido foi '400' sem mensagem

@simula_proposta_a_vista_data_pagamento_com_string
Cenário: Simulando proposta à vista com a data de pagamento com valor string
    Dado que um dos contratos listados com o campo "canNegotiate" equivalente a "true" seja selecionado
    Quando a requisição de simulação de proposta for realizada à vista com a data de pagamento com um valor sring
    Então devo verificar se o status obtido foi '400' sem mensagem

@simula_proposta_contrato_incorreto
Cenário: Simulando proposta informando um número de contrato incorreto
    Dado que o contrato '15154513513' seja utilizado 
    Quando a requisição de simulação de proposta for realizada com o número de contrato incorreto
    Então devo verificar se o status obtido foi '200' com a mensagem 'O cliente não tem acesso ao contrato'

@simula_proposta_contrato_vazio
Cenário: Simulando proposta informando um número de contrato vazio
    Dado que o contrato '' seja utilizado 
    Quando a requisição de simulação de proposta for realizada com o número de contrato vazio
    Então devo verificar se o status obtido foi '' sem mensagem 

@simula_proposta_contrato_nulo
Cenário: Simulando proposta informando um número de contrato nulo
    Dado que o contrato '0' seja utilizado 
    Quando a requisição de simulação de proposta for realizada com o número de contrato nulo
    Então devo verificar se o status obtido foi '200' com a mensagem 'O cliente não tem acesso ao contrato'

@simula_proposta_contrato_string
Cenário: Simulando proposta informando uma string no número de contrato
    Dado que o contrato 'gfgdfjgtfu' seja utilizado 
    Quando a requisição de simulação de proposta for realizada informando uma string no número de contrato
    Então devo verificar se o status obtido foi '200' com a mensagem 'O cliente não tem acesso ao contrato'

@inclui_acordo_a_vista_valor_nulo
Cenário: Incluindo proposta à vista com valor nulo
    Dado que um o contrato simulado anteriormente seja selecionado 
    Quando a requisição de inclusão de proposta for realizada à vista com um valor nulo
    Então devo verificar se o status obtido na inclusão de acordo foi '200' com o status 'PENDING'

@inclui_acordo_a_vista_valor_vazio
Cenário: Incluindo proposta à vista com valor vazio
    Dado que um o contrato simulado anteriormente seja selecionado
    Quando a requisição de inclusão de proposta for realizada à vista com um valor vazio
    Então devo verificar se o status obtido na inclusão de acordo foi '500' sem mensagem

@inclui_acordo_a_vista_valor_com_string
Cenário: Incluindo proposta à vista com valor string
    Dado que um o contrato simulado anteriormente seja selecionado
    Quando a requisição de inclusão de proposta for realizada à vista com um valor string
    Então devo verificar se o status obtido na inclusão de acordo foi '400' sem mensagem

@inclui_acordo_a_vista_data_pagamento_nulo
Cenário: Incluindo proposta à vista com a data de pagamento nula
    Dado que um o contrato simulado anteriormente seja selecionado
    Quando a requisição de inclusão de proposta for realizada à vista com a data de pagamento nula
    Então devo verificar se o status code obtido na inclusão de acordo foi '200' com a mensagem '-10003 - Data de pagamento menor que a data atual.'

@inclui_acordo_a_vista_data_pagamento_vazio
Cenário: Incluindo proposta à vista com a data de pagamento vazia
    Dado que um o contrato simulado anteriormente seja selecionado
    Quando a requisição de inclusão de proposta for realizada à vista com a data de pagamento vazia
    Então devo verificar se o status obtido na inclusão de acordo foi '400' sem mensagem

@inclui_acordo_a_vista_data_pagamento_com_string
Cenário: Incluindo proposta à vista com a data de pagamento com valor string
    Dado que um o contrato simulado anteriormente seja selecionado
    Quando a requisição de inclusão de proposta for realizada à vista com a data de pagamento com um valor sring
    Então devo verificar se o status obtido na inclusão de acordo foi '400' sem mensagem

@inclui_acordo_contrato_incorreto
Cenário: Incluindo proposta informando um número de contrato incorreto
    Dado que o contrato '15154513513' seja utilizado 
    Quando a requisição de inclusão de proposta for realizada com o número de contrato incorreto
    Então devo verificar se o status code obtido na inclusão de acordo foi '200' com a mensagem 'O cliente não tem acesso ao contrato'

@inclui_acordo_contrato_vazio
Cenário: Incluindo proposta informando um número de contrato vazio
    Dado que o contrato '' seja utilizado 
    Quando a requisição de inclusão de proposta for realizada com o número de contrato vazio
    Então devo verificar se o status obtido na inclusão de acordo foi '' sem mensagem 

@inclui_acordo_contrato_nulo
Cenário: Incluindo proposta informando um número de contrato nulo
    Dado que o contrato '0' seja utilizado 
    Quando a requisição de inclusão de proposta for realizada com o número de contrato nulo
    Então devo verificar se o status code obtido na inclusão de acordo foi '200' com a mensagem 'O cliente não tem acesso ao contrato'

@inclui_acordo_contrato_string
Cenário: Incluindo proposta informando uma string no número de contrato
    Dado que o contrato 'gfgdfjgtfu' seja utilizado
    Quando a requisição de inclusão de proposta for realizada informando uma string  no número de contrato
    Então devo verificar se o status code obtido na inclusão de acordo foi '200' com a mensagem 'O cliente não tem acesso ao contrato'

@simula_proposta_a_prazo_valor_baixo
Cenário: Simulando proposta a prazo com valor baixo
    Dado que um dos contratos listados com o campo "canNegotiate" equivalente a "true" seja selecionado
    Quando a requisição de simulação de proposta for realizada a prazo com um valor baixo 
    Então devo verificar se o status obtido foi '200' com o status 'PENDING'

@simula_proposta_a_prazo_valor_alto
Cenário: Simulando proposta a prazo com valor alto 
    Dado que um dos contratos listados com o campo "canNegotiate" equivalente a "true" seja selecionado
    Quando a requisição de simulação de proposta for realizada a prazo com um valor alto
    Então devo verificar se o status obtido foi '200' com o status 'ACCEPTED'

@executa_quebra_de_acordos
@inclui_acordo_a_prazo_valor_alto
Cenário: Incluindo proposta a prazo com valor alto
    Dado que um o contrato simulado anteriormente seja selecionado
    Quando a requisição de inclusão de proposta for realizada a prazo com o mesmo valor alto da simulação
    Então devo verificar se o status obtido na inclusão de acordo foi '200' com o status 'ACCEPTED'

@simula_proposta_a_prazo_informando_data_invalida
Cenário: Simulando proposta a prazo com uma data anterior ao dia atual
    Dado que um dos contratos listados com o campo "canNegotiate" equivalente a "true" seja selecionado
    Quando a requisição de simulação de proposta for realizada a prazo com uma data anterior ao dia atual
    Então devo verificar se o status code obtido foi '200' com a mensagem '-10003 - Data de pagamento menor que a data atual.'

@inclui_acordo_a_prazo_informando_data_invalida
Cenário: Incluindo proposta a prazo com uma data anterior ao dia atual
    Dado que um o contrato simulado anteriormente seja selecionado
    Quando a requisição de inclusão de proposta for realizada a prazo com uma data anterior ao dia atual
    Então devo verificar se o status code obtido na inclusão de acordo foi '200' com a mensagem 'DATA DO VENCIMENTO DO PRIMEIRO BOLETO NAO PODE SER MENOR QUE A DATA ATUAL' 

@simula_proposta_a_prazo_valor_abaixo_minimo
Cenário: Simulando proposta a prazo com valor menor que dez reais
    Dado que um dos contratos listados com o campo "canNegotiate" equivalente a "true" seja selecionado
    Quando a requisição de simulação de proposta for realizada a prazo com o valor de '2.00' reais
    Então devo verificar se o status obtido foi '200' com o status 'PENDING'

@inclui_acordo_a_prazo_valor_abaixo_minimo
Cenário: Incluindo proposta a prazo com valor menor que dez reais
    Dado que um o contrato simulado anteriormente seja selecionado
    Quando a requisição de inclusão de proposta for realizada a prazo com o mesmo valor de '2.00' reais da simulação
    Então devo verificar se o status obtido na inclusão de acordo foi '200' com o status 'PENDING'

@simula_proposta_a_prazo_valor_minimo
Cenário: Simulando proposta a prazo com valor de dez reais
    Dado que um dos contratos listados com o campo "canNegotiate" equivalente a "true" seja selecionado
    Quando a requisição de simulação de proposta for realizada a prazo com o valor de '10,00' reais
    Então devo verificar se o status obtido foi '200' com o status 'PENDING'

@inclui_acordo_a_prazo_valor_minimo
Cenário: Incluindo proposta a prazo com valor de dez reais
    Dado que um o contrato simulado anteriormente seja selecionado
    Quando a requisição de inclusão de proposta for realizada a prazo com o mesmo valor de '10,00' reais da simulação
    Então devo verificar se o status obtido na inclusão de acordo foi '200' com o status 'PENDING' 

@simula_proposta_a_prazo_acima_do_limite_de_parcelas
Cenário: Simulando proposta a prazo acima do limite de parcelamento
    Dado que um dos contratos listados com o campo "canNegotiate" equivalente a "true" seja selecionado
    Quando a requisição de simulação de proposta for realizada parcelada em '40' vezes
    Então devo verificar se o status obtido foi '200' com o status 'REFUSED' 

@inclui_acordo_a_prazo_acima_do_limite_de_parcelas
Cenário: Incluindo proposta a prazo acima do limite de parcelamento
    Dado que um o contrato simulado anteriormente seja selecionado
    Quando a requisição de inclusão de proposta for realizada parcelada em '40' vezes
    Então devo verificar se o status code obtido na inclusão de acordo foi '200' com a mensagem 'FORMA DE PAGAMENTO MAIOR QUE'

@simula_proposta_a_prazo_entrada_nula_parcela_nula
Cenário: Simulando proposta a prazo com valor de entrada e da parcela nulos
    Dado que um dos contratos listados com o campo "canNegotiate" equivalente a "true" seja selecionado
    Quando a requisição de simulação de proposta for realizada a prazo com valor de entrada e da parcela nulos
    Então devo verificar se o status obtido foi '200' com o status 'PENDING'

@simula_proposta_a_prazo_entrada_nula
Cenário: Simulando proposta a prazo com valor de entrada nula
    Dado que um dos contratos listados com o campo "canNegotiate" equivalente a "true" seja selecionado
    Quando a requisição de simulação de proposta for realizada a prazo com valor de entrada nula
    Então devo verificar se o status obtido foi '200' com o status 'ACCEPTED'

@simula_proposta_a_prazo_parcela_nula
Cenário: Simulando proposta a prazo com o valor da parcela nulo
    Dado que um dos contratos listados com o campo "canNegotiate" equivalente a "true" seja selecionado
    Quando a requisição de simulação de proposta for realizada a prazo com o valor da parcela nula
    Então devo verificar se o status obtido foi '200' com o status 'ACCEPTED'

@simula_proposta_a_prazo_valor_vazio
Cenário: Simulando proposta a prazo com valor vazio
    Dado que um dos contratos listados com o campo "canNegotiate" equivalente a "true" seja selecionado
    Quando a requisição de simulação de proposta for realizada a prazo com um valor vazio
    Então devo verificar se o status obtido foi '500' sem mensagem

@simula_proposta_a_prazo_valor_com_string
Cenário: Simulando proposta a prazo com valor string
    Dado que um dos contratos listados com o campo "canNegotiate" equivalente a "true" seja selecionado
    Quando a requisição de simulação de proposta for realizada a prazo com um valor string
    Então devo verificar se o status obtido foi '400' sem mensagem

@simula_proposta_a_prazo_data_pagamento_nulo
Cenário: Simulando proposta a prazo com a data de pagamento nula
    Dado que um dos contratos listados com o campo "canNegotiate" equivalente a "true" seja selecionado
    Quando a requisição de simulação de proposta for realizada a prazo com a data de pagamento nula
    Então devo verificar se o status obtido foi '200' com a mensagem '-10003 - Data de pagamento menor que a data atual.'

@simula_proposta_a_prazo_data_pagamento_vazio
Cenário: Simulando proposta a prazo com a data de pagamento vazia
    Dado que um dos contratos listados com o campo "canNegotiate" equivalente a "true" seja selecionado
    Quando a requisição de simulação de proposta for realizada a prazo com a data de pagamento vazia
    Então devo verificar se o status obtido foi '400' sem mensagem

@simula_proposta_a_prazo_data_pagamento_com_string
Cenário: Simulando proposta a prazo com a data de pagamento com valor string
    Dado que um dos contratos listados com o campo "canNegotiate" equivalente a "true" seja selecionado
    Quando a requisição de simulação de proposta for realizada a prazo com a data de pagamento com um valor sring
    Então devo verificar se o status obtido foi '400' sem mensagem

@inclui_acordo_a_prazo_valor_vazio
Cenário: Incluindo proposta a prazo com valor vazio
    Dado que um o contrato simulado anteriormente seja selecionado
    Quando a requisição de inclusão de proposta for realizada a prazo com um valor vazio
    Então devo verificar se o status obtido na inclusão de acordo foi '500' sem mensagem

@inclui_acordo_a_prazo_valor_com_string
Cenário: Incluindo proposta a prazo com valor string
    Dado que um o contrato simulado anteriormente seja selecionado
    Quando a requisição de inclusão de proposta for realizada a prazo com um valor string
    Então devo verificar se o status obtido na inclusão de acordo foi '400' sem mensagem

@inclui_acordo_a_prazo_data_pagamento_nulo
Cenário: Incluindo proposta a prazo com a data de pagamento nula
    Dado que um o contrato simulado anteriormente seja selecionado
    Quando a requisição de inclusão de proposta for realizada a prazo com a data de pagamento nula
    Então devo verificar se o status code obtido na inclusão de acordo foi '200' com a mensagem '-10003 - Data de pagamento menor que a data atual.'

@inclui_acordo_a_prazo_data_pagamento_vazio
Cenário: Incluindo proposta a prazo com a data de pagamento vazia
    Dado que um o contrato simulado anteriormente seja selecionado
    Quando a requisição de inclusão de proposta for realizada a prazo com a data de pagamento vazia
    Então devo verificar se o status obtido na inclusão de acordo foi '400' sem mensagem

@inclui_acordo_a_prazo_data_pagamento_com_string
Cenário: Incluindo proposta a prazo com a data de pagamento com valor string
    Dado que um o contrato simulado anteriormente seja selecionado
    Quando a requisição de inclusão de proposta for realizada a prazo com a data de pagamento com um valor sring
    Então devo verificar se o status obtido na inclusão de acordo foi '400' sem mensagem

@inclui_acordo_a_prazo_entrada_nula_parcela_nula
Cenário: Incluindo proposta a prazo com valor de entrada e da parcela nulos
    Dado que um o contrato simulado anteriormente seja selecionado
    Quando a requisição de inclusão de proposta for realizada a prazo com valor de entrada e da parcela nulos
    Então devo verificar se o status obtido na inclusão de acordo foi '500' sem mensagem

@inclui_acordo_a_prazo_parcela_nula
Cenário: Incluindo proposta a prazo com valor nulo
    Dado que um o contrato simulado anteriormente seja selecionado
    Quando a requisição de inclusão de proposta for realizada a prazo com o valor da parcela nula
    Então devo verificar se o status code obtido na inclusão de acordo foi '200' com a mensagem 'INFORME VALOR DA PARCELA.'

@executa_quebra_de_acordos
@inclui_acordo_a_prazo_entrada_nula
Cenário: Incluindo proposta a prazo com valor de entrada nula
    Dado que um o contrato simulado anteriormente seja selecionado
    Quando a requisição de inclusão de proposta for realizada a prazo com o valor de entrada nula
    Então devo verificar se o status obtido na inclusão de acordo foi '200' com o status 'ACCEPTED'