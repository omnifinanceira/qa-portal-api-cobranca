#language: pt

@login_api
Funcionalidade: Login no Portal Negociação
- Para que eu possa fazer a autenticação da API sendo um usuário
- Para que eu possa validar o login da API

@login_sucesso
Cenário: Realizar login com sucesso
    Quando eu preencher o campo de usuário "02416854275"
    E o campo de senha "senha123"
    E fazer a requisição
    Então devo verificar se o status obtido foi '200'

@login_erro_01
Cenário: Realizar login com usuário inconsistente
    Quando eu preencher o campo de usuário "54222"
    E o campo de senha "senha@123"
    E fazer a requisição
    Então devo verificar se o status obtido foi '401' com a descrição "Invalid user credentials"

@login_erro_02
Cenário: Realizar login com senha inconsistente
    Quando eu preencher o campo de usuário "89577280234"
    E o campo de senha "aaaaa"
    E fazer a requisição
    Então devo verificar se o status obtido foi '401' com a descrição "Invalid user credentials"