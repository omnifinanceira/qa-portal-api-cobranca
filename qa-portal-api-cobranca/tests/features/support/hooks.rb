After '@executa_quebra_de_acordos' do 
    require_relative '../pages/autenticacao_omnifacil.rb'
    require_relative '../pages/contratos.rb'
    require_relative '../pages/dados_cliente.rb'
    require_relative '../pages/quebra_acordo.rb'
    
    $aut = AutenticacaoOmnifacil.new
    $dados_cliente = DadosCliente.new
    $contratos = Contratos.new
    $quebra_de_acordo = QuebraAcordo.new

    token =  $aut.retorna_token
    $busca_contratos = $contratos.busca_contratos($token)
    contrato = $busca_contratos['contracts'][0]['id']
    nr_seq_acordo = $busca_contratos['contracts'][0]['agreements'][0]['sequenceNumber']
    $busca_dados = $dados_cliente.busca_dados($token)
    cliente = $busca_dados['client']['codigoCliente']
    $quebra_acordo = $quebra_de_acordo.quebra_acordo(token, contrato, cliente, nr_seq_acordo)

end