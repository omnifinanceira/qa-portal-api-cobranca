require_relative '../pages/util.rb'

class Request < Util

    def initialize
        @url_padrao = CONFIG['url_geral']
    end

    def exec_post_aut_portal(body)
        url_full = "https://hmg-omni-auth.omni.com.br/auth/realms/cliente-omni/protocol/openid-connect/token"

        response = HTTParty.post(url_full,
        :body => body,
        :headers => {'Content-Type' => 'application/x-www-form-urlencoded',
            'Accept' => 'application/x-www-form-urlencoded' })

        $cookie = response.headers['set-cookie']

        unless response['Erros'].nil?
            gravar_request_response(url_full, body, response)
        end

        return response
    end

    def exec_post_aut_omnifacil(body)
        url_full = "http://hml-omnifacil2.omni.com.br/omni-ws/api/auth/user"

        response = HTTParty.post(url_full,
        :body => body.to_json,
        :headers => {'Content-Type' => 'application/json;charset=UTF-8',
            'Accept' => 'application/json;charset=UTF-8' })

        $cookie = response.headers['set-cookie']

        unless response['Erros'].nil?
            gravar_request_response(url_full, body, response)
        end

        return response
    end

    def exec_delete(path, token)
        url_full = "https://hmg-acordo.omni.com.br#{path}"

        response = HTTParty.delete(url_full,
        :headers => { 'Content-Type' => 'application/json;charset=UTF-8',
            'Accept' => 'application/json;charset=UTF-8',
            'omni-autenticacao' => {"login": "320DANILOG", "token": "#{token}"}.to_json })
        
        return response
    end 

    def exec_post(path, body, token)
        url_full = "#{@url_padrao}#{path}"

        response = HTTParty.post(url_full,
        :body => body.to_json,
        :headers => { 'Content-Type' => 'application/json;charset=UTF-8',
            'Accept' => 'application/json;charset=UTF-8',
            "Authorization" => "Bearer #{token}"})

        unless response['Erros'].nil?
            gravar_request_response(url_full, body, response)
        end

        return response
    end

    def exec_get(path, token)
        url_full = "#{@url_padrao}#{path}"

        response = HTTParty.get(url_full,
        :headers => { 'Content-Type' => 'application/json;charset=UTF-8',
            'Accept' => 'application/json;charset=UTF-8',
            "Authorization" => "Bearer #{token}"})
        
        return response
    end 
end