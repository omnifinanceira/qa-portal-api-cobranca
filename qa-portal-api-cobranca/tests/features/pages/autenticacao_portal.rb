class AutenticacaoPortal < Request

    def fazer_autenticacao(usuario = $usuario, senha = $senha)

        body = {
            "username": usuario,
            "password": senha,
            "scope": "openid portal-negociacao",
            "client_secret": "c33d5b13-771c-4a3d-bcbb-1b87dad74e30",
            "client_id": "acerto",
            "grant_type": "password"
        }

        autenticacao = exec_post_aut_portal(body)
        return autenticacao
    end

    def retorna_token 
        token = fazer_autenticacao['access_token']
        return token
    end

end