class QuebraAcordo < Request

    def quebra_acordo(token, contrato, cliente, nr_seq_acordo)
        acordo = exec_delete("/api/contratos/#{contrato}/clientes/#{cliente}/acordos/#{nr_seq_acordo}", token)
        return acordo
    end

end