class SimulaProposta < Request

    def simula_proposta(token, contrato, valor_entrada, data, parcelas, valor_parcela)

        body = {
            "downValue": valor_entrada,
            "firstPaymentDate": data,
            "installmentsQuantity": parcelas,
            "installmentsValue": valor_parcela
        }

        simulacao = exec_post("/api/clients/contracts/#{contrato}/agreements/simulation", body, token)
        return simulacao
    end

end