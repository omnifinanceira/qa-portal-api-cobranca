class DadosCliente < Request

    def busca_dados(token)
        dados = exec_get("/api/clients", token)
        return dados
    end

end