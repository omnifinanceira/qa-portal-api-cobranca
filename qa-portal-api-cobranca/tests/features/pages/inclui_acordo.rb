class IncluiAcordo < Request

    def inclui_acordo(token, contrato, valor_entrada, data, parcelas, valor_parcela)

        body = {
            "downValue": valor_entrada,
            "firstPaymentDate": data,
            "installmentsQuantity": parcelas,
            "installmentsValue": valor_parcela
        }

        inclusao = exec_post("/api/clients/contracts/#{contrato}/agreements", body, token)
        return inclusao
    end

end