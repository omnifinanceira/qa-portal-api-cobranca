$contratos = Contratos.new
$simulacao_proposta = SimulaProposta.new
$inclusao_acordo = IncluiAcordo.new
Dado('que eu faça a requisição para buscar os contratos de um CPF') do
    util = Util.new
    $data = util.gera_data_hoje
    $busca_contratos = $contratos.busca_contratos($token)
end

Então('devo verificar se a requisição de busca de contratos retornou os campos {string}, {string} e {string}') do |valor_divida, qt_max_parcelas, contrato|
    valor_divida = $busca_contratos['contracts'][0]['debit']['correctedValue']
    qt_max_parcelas = $busca_contratos['contracts'][0]['negotiationRules']['installmentRules']['maxAmount']
    contrato = $busca_contratos['contracts'][0]['id']

    $dados_contrato = []
    $dados_contrato << valor_divida 
    $dados_contrato << qt_max_parcelas
    $dados_contrato << contrato
end

Dado('que um dos contratos listados com o campo {string} equivalente a {string} seja selecionado') do |canNegotiate, status|
    $busca_contratos = $contratos.busca_contratos($token)
    $canNegotiate = $busca_contratos['contracts'][0]['negotiationRules']['canNegotiate'].to_s
    expect($canNegotiate).to eq(status)
end

Quando('a requisição de simulação de proposta for realizada à vista com um valor baixo') do
    parcelas = 1
    valor_parcela = 0
    valor_divida = $dados_contrato[0]
    contrato = $dados_contrato[2]
    valor_entrada = (valor_divida*0.01).to_f
    $simula_proposta = $simulacao_proposta.simula_proposta($token, contrato, valor_entrada, $data, parcelas, valor_parcela)

    $valor_simulacao_proposta = []
    $valor_simulacao_proposta << valor_entrada
end

Então('devo verificar se o status obtido foi {string} com o status {string}') do |status1, status2|
    status_code = (@response_login.code).to_s
    expect(status_code).to eq(status1)
    request_status = $simula_proposta['simulation']['status']
    expect(request_status.include?(status2)).to be true
end

Dado('que um o contrato simulado anteriormente seja selecionado') do
    $contrato = $dados_contrato[2]
end

Quando('a requisição de inclusão de proposta for realizada à vista com o mesmo valor baixo da simulação') do
    valor_entrada = $valor_simulacao_proposta[0]
    parcelas = 1
    valor_parcela = 0
    $inclui_acordo = $inclusao_acordo.inclui_acordo($token, $contrato, valor_entrada, $data, parcelas, valor_parcela)
end

Então('devo verificar se o status obtido na inclusão de acordo foi {string} com o status {string}') do |status1, status2|
    status_code = (@response_login.code).to_s
    expect(status_code).to eq(status1)
    request_status = $inclui_acordo['result']['status']
    expect(request_status.include?(status2)).to be true
end

Quando('a requisição de simulação de proposta for realizada à vista com um valor alto') do
    parcelas = 1
    valor_parcela = 0
    valor_divida = $dados_contrato[0]
    contrato = $dados_contrato[2]
    valor_entrada = (valor_divida*0.8).to_f
    $simula_proposta = $simulacao_proposta.simula_proposta($token, contrato, valor_entrada, $data, parcelas, valor_parcela)

    $valor_simulacao_proposta = []
    $valor_simulacao_proposta << valor_entrada
end

Quando('a requisição de inclusão de proposta for realizada à vista com o mesmo valor alto da simulação') do
    valor_entrada = $valor_simulacao_proposta[0]
    parcelas = 1
    valor_parcela = 0
    $inclui_acordo = $inclusao_acordo.inclui_acordo($token, $contrato, valor_entrada, $data, parcelas, valor_parcela)
end

Quando('a requisição de simulação de proposta for realizada à vista com uma data anterior ao dia atual') do
    data = Time.parse("2021-01-07 13:54:58")
    data_anterior = data.to_f * 1000
    data_anterior.to_i
    parcelas = 1
    valor_parcela = 0
    contrato = $dados_contrato[2]
    valor_divida = $dados_contrato[0]
    valor_entrada = (valor_divida*0.8).to_f
    $simula_proposta = $simulacao_proposta.simula_proposta($token, contrato, valor_entrada, data_anterior, parcelas, valor_parcela)
end

Então('devo verificar se o status code obtido foi {string} com a mensagem {string}') do |status, mensagem|
    status_code = (@response_login.code).to_s
    expect(status_code).to eq(status)
    request_mensagem = $simula_proposta['messages']
    expect(request_mensagem.include?(mensagem)).to be true
end

Então('devo verificar se o status obtido foi {string} com a mensagem {string}') do |status, mensagem|
    status_code = (@response_login.code).to_s
    expect(status_code).to eq(status)
    request_mensagem = $simula_proposta['messages']
    expect(request_mensagem.include?(mensagem)).to be true
end

Quando('a requisição de inclusão de proposta for realizada à vista com uma data anterior ao dia atual') do
    data = Time.parse("2021-01-07 13:54:58")
    data_anterior = data.to_f * 1000
    data_anterior.to_i
    valor_entrada = $valor_simulacao_proposta[0]
    parcelas = 1
    valor_parcela = 0
    $inclui_acordo = $inclusao_acordo.inclui_acordo($token, $contrato, valor_entrada, data_anterior, parcelas, valor_parcela)
end

Então('devo verificar se o status code obtido na inclusão de acordo foi {string} com a mensagem {string}') do |status, mensagem|
    status_code = (@response_login.code).to_s
    expect(status_code).to eq(status)
    request_mensagem = $inclui_acordo['messages'][0]
    expect(request_mensagem.include?(mensagem)).to be true
end

Quando('a requisição de simulação de proposta for realizada à vista com o valor de {string} reais') do |entrada|
    parcelas = 1
    valor_parcela = 0
    contrato = $dados_contrato[2]
    valor_entrada = entrada.to_i
    $simula_proposta = $simulacao_proposta.simula_proposta($token, contrato, valor_entrada, $data, parcelas, valor_parcela)
end

Quando('a requisição de inclusão de proposta for realizada à vista com o mesmo valor de {string} reais da simulação') do |entrada|
    valor_entrada = entrada.to_i
    parcelas = 1
    valor_parcela = 0
    $inclui_acordo = $inclusao_acordo.inclui_acordo($token, $contrato, valor_entrada, $data, parcelas, valor_parcela)
end

Quando('a requisição de simulação de proposta for realizada à vista com um valor nulo') do
    parcelas = 1
    valor_parcela = 0
    contrato = $dados_contrato[2]
    valor_entrada = 0
    $simula_proposta = $simulacao_proposta.simula_proposta($token, contrato, valor_entrada, $data, parcelas, valor_parcela)
end

Quando('a requisição de simulação de proposta for realizada à vista com um valor vazio') do
    parcelas = 1
    valor_parcela = 0
    contrato = $dados_contrato[2]
    valor_entrada = ""
    $simula_proposta = $simulacao_proposta.simula_proposta($token, contrato, valor_entrada, $data, parcelas, valor_parcela)
end

Então('devo verificar se o status obtido foi {string} sem mensagem') do |status|
    request_status = $simula_proposta['status'].to_s
    expect(request_status).to eq(status)
end

Quando('a requisição de simulação de proposta for realizada à vista com um valor string') do
    parcelas = 1
    valor_parcela = 0
    contrato = $dados_contrato[2]
    valor_entrada = "hhyykjjjy"
    $simula_proposta = $simulacao_proposta.simula_proposta($token, contrato, valor_entrada, $data, parcelas, valor_parcela)
end

Quando('a requisição de simulação de proposta for realizada à vista com a data de pagamento nula') do
    parcelas = 1
    data = 0
    valor_parcela = 0
    valor_divida = $dados_contrato[0]
    contrato = $dados_contrato[2]
    valor_entrada = (valor_divida*0.8).to_f
    $simula_proposta = $simulacao_proposta.simula_proposta($token, contrato, valor_entrada, data, parcelas, valor_parcela)
end

Quando('a requisição de simulação de proposta for realizada à vista com a data de pagamento vazia') do
    parcelas = 1
    data = ""
    valor_parcela = 0
    valor_divida = $dados_contrato[0]
    contrato = $dados_contrato[2]
    valor_entrada = (valor_divida*0.8).to_f
    $simula_proposta = $simulacao_proposta.simula_proposta($token, contrato, valor_entrada, data, parcelas, valor_parcela)
end

Quando('a requisição de simulação de proposta for realizada à vista com a data de pagamento com um valor sring') do
    parcelas = 1
    data = "kfkkrfgj"
    valor_parcela = 0
    valor_divida = $dados_contrato[0]
    contrato = $dados_contrato[2]
    valor_entrada = (valor_divida*0.8).to_f
    $simula_proposta = $simulacao_proposta.simula_proposta($token, contrato, valor_entrada, data, parcelas, valor_parcela)
end

Dado('que o contrato {string} seja utilizado') do |contrato|
    $contrato_incorreto = []
    $contrato_incorreto << contrato
end

Quando('a requisição de simulação de proposta for realizada com o número de contrato incorreto') do
    parcelas = 1
    valor_parcela = 0
    valor_divida = 20
    contrato = $contrato_incorreto[0]
    valor_entrada = (valor_divida*0.8).to_f
    $simula_proposta = $simulacao_proposta.simula_proposta($token, contrato, valor_entrada, $data, parcelas, valor_parcela)
end

Quando('a requisição de simulação de proposta for realizada com o número de contrato vazio') do
    parcelas = 1
    valor_parcela = 0
    valor_divida = 20
    contrato = ''
    valor_entrada = (valor_divida*0.8).to_f
    $simula_proposta = $simulacao_proposta.simula_proposta($token, contrato, valor_entrada, $data, parcelas, valor_parcela)
end

Quando('a requisição de simulação de proposta for realizada com o número de contrato nulo') do
    parcelas = 1
    valor_parcela = 0
    valor_divida = 20
    contrato = 0
    valor_entrada = (valor_divida*0.8).to_f
    $simula_proposta = $simulacao_proposta.simula_proposta($token, contrato, valor_entrada, $data, parcelas, valor_parcela)
end

Quando('a requisição de simulação de proposta for realizada informando uma string no número de contrato') do
    parcelas = 1
    valor_parcela = 0
    valor_divida = 20
    contrato = 'jnjnjnknj'
    valor_entrada = (valor_divida*0.8).to_f
    $simula_proposta = $simulacao_proposta.simula_proposta($token, contrato, valor_entrada, $data, parcelas, valor_parcela)
end

Quando('a requisição de inclusão de proposta for realizada com o número de contrato incorreto') do
    valor_entrada = 20
    parcelas = 1
    valor_parcela = 0
    contrato = $contrato_incorreto[0]
    $inclui_acordo = $inclusao_acordo.inclui_acordo($token, contrato, valor_entrada, $data, parcelas, valor_parcela)
end

Quando('a requisição de inclusão de proposta for realizada com o número de contrato vazio') do
    valor_entrada = 20
    parcelas = 1
    valor_parcela = 0
    contrato = ''
    $inclui_acordo = $inclusao_acordo.inclui_acordo($token, contrato, valor_entrada, $data, parcelas, valor_parcela)
end

Quando('a requisição de inclusão de proposta for realizada com o número de contrato nulo') do
    valor_entrada = 20
    parcelas = 1
    valor_parcela = 0
    contrato = 0
    $inclui_acordo = $inclusao_acordo.inclui_acordo($token, contrato, valor_entrada, $data, parcelas, valor_parcela)
end

Quando('a requisição de inclusão de proposta for realizada informando uma string  no número de contrato') do
    valor_entrada = 20
    parcelas = 1
    valor_parcela = 0
    contrato = 'jjgjuju'
    $inclui_acordo = $inclusao_acordo.inclui_acordo($token, contrato, valor_entrada, $data, parcelas, valor_parcela)
end

Então('devo verificar se o status obtido na inclusão de acordo foi {string} sem mensagem') do |status|
    request_status = $inclui_acordo['status'].to_s
    expect(request_status).to eq(status)
end

Quando('a requisição de inclusão de proposta for realizada à vista com um valor nulo') do
    valor_entrada = 0
    parcelas = 1
    valor_parcela = 0
    $inclui_acordo = $inclusao_acordo.inclui_acordo($token, $contrato, valor_entrada, $data, parcelas, valor_parcela)
end

Quando('a requisição de inclusão de proposta for realizada à vista com um valor vazio') do
    valor_entrada = ""
    parcelas = 1
    valor_parcela = 0
    $inclui_acordo = $inclusao_acordo.inclui_acordo($token, $contrato, valor_entrada, $data, parcelas, valor_parcela)
end

Quando('a requisição de inclusão de proposta for realizada à vista com um valor string') do
    valor_entrada = "frfrrfrf"
    parcelas = 1
    valor_parcela = 0
    $inclui_acordo = $inclusao_acordo.inclui_acordo($token, $contrato, valor_entrada, $data, parcelas, valor_parcela)
end

Quando('a requisição de inclusão de proposta for realizada à vista com a data de pagamento nula') do
    valor_entrada = $valor_simulacao_proposta[0]
    data = 0
    parcelas = 1
    valor_parcela = 0
    $inclui_acordo = $inclusao_acordo.inclui_acordo($token, $contrato, valor_entrada, data, parcelas, valor_parcela)
end

Quando('a requisição de inclusão de proposta for realizada à vista com a data de pagamento vazia') do
    valor_entrada = $valor_simulacao_proposta[0]
    data = ""
    parcelas = 1
    valor_parcela = 0
    $inclui_acordo = $inclusao_acordo.inclui_acordo($token, $contrato, valor_entrada, data, parcelas, valor_parcela)
end

Quando('a requisição de inclusão de proposta for realizada à vista com a data de pagamento com um valor sring') do
    valor_entrada = $valor_simulacao_proposta[0]
    data = "fknnfnnf"
    parcelas = 1
    valor_parcela = 0
    $inclui_acordo = $inclusao_acordo.inclui_acordo($token, $contrato, valor_entrada, data, parcelas, valor_parcela)
end

Quando('a requisição de simulação de proposta for realizada a prazo com um valor baixo') do
    parcelas = 4
    valor_divida = $dados_contrato[0]
    contrato = $dados_contrato[2]
    valor_entrada = (valor_divida*0.01/parcelas).to_f
    valor_parcela = valor_entrada
    $simula_proposta = $simulacao_proposta.simula_proposta($token, contrato, valor_entrada, $data, parcelas, valor_parcela)
end

Quando('a requisição de inclusão de proposta for realizada a prazo com o mesmo valor baixo da simulação') do
    valor_entrada = $valor_simulacao_proposta[0]
    parcelas = 4
    valor_parcela = valor_entrada
    $inclui_acordo = $inclusao_acordo.inclui_acordo($token, $contrato, valor_entrada, $data, parcelas, valor_parcela)
end

Quando('a requisição de simulação de proposta for realizada a prazo com um valor alto') do
    parcelas = 4
    valor_divida = $dados_contrato[0]
    contrato = $dados_contrato[2]
    valor_entrada = (valor_divida*0.8/parcelas).to_f
    valor_parcela = valor_entrada
    $simula_proposta = $simulacao_proposta.simula_proposta($token, contrato, valor_entrada, $data, parcelas, valor_parcela)
end

Quando('a requisição de inclusão de proposta for realizada a prazo com o mesmo valor alto da simulação') do
    valor_entrada = $valor_simulacao_proposta[0]
    parcelas = 4
    valor_parcela = valor_entrada
    $inclui_acordo = $inclusao_acordo.inclui_acordo($token, $contrato, valor_entrada, $data, parcelas, valor_parcela)
end

Quando('a requisição de simulação de proposta for realizada a prazo com uma data anterior ao dia atual') do
    data = Time.parse("2021-01-07 13:54:58")
    data_anterior = data.to_f * 1000
    data_anterior.to_i
    parcelas = 4
    valor_divida = $dados_contrato[0]
    contrato = $dados_contrato[2]
    valor_entrada = (valor_divida*0.8/parcelas).to_f
    valor_parcela = valor_entrada
    $simula_proposta = $simulacao_proposta.simula_proposta($token, contrato, valor_entrada, data_anterior, parcelas, valor_parcela)
end

Quando('a requisição de inclusão de proposta for realizada a prazo com uma data anterior ao dia atual') do
    data = Time.parse("2021-01-07 13:54:58")
    data_anterior = data.to_f * 1000
    data_anterior.to_i
    valor_entrada = $valor_simulacao_proposta[0]
    parcelas = 4
    valor_parcela = valor_entrada
    $inclui_acordo = $inclusao_acordo.inclui_acordo($token, $contrato, valor_entrada, data_anterior, parcelas, valor_parcela)
end

Quando('a requisição de simulação de proposta for realizada a prazo com o valor de {string} reais') do |entrada|
    parcelas = 2
    contrato = $dados_contrato[2]
    valor_entrada = entrada.to_i
    valor_parcela = valor_entrada
    $simula_proposta = $simulacao_proposta.simula_proposta($token, contrato, valor_entrada, $data, parcelas, valor_parcela)
end

Quando('a requisição de inclusão de proposta for realizada a prazo com o mesmo valor de {string} reais da simulação') do |entrada|
    valor_entrada = entrada.to_i
    parcelas = 2
    valor_parcela = valor_entrada
    $inclui_acordo = $inclusao_acordo.inclui_acordo($token, $contrato, valor_entrada, $data, parcelas, valor_parcela)
end

Quando('a requisição de simulação de proposta for realizada parcelada em {string} vezes') do |num_parcelas|
    parcelas = num_parcelas.to_i
    valor_divida = $dados_contrato[0]
    contrato = $dados_contrato[2]
    valor_entrada = (valor_divida*0.8/parcelas).to_f
    valor_parcela = valor_entrada
    $simula_proposta = $simulacao_proposta.simula_proposta($token, contrato, valor_entrada, $data, parcelas, valor_parcela)
end

Quando('a requisição de inclusão de proposta for realizada parcelada em {string} vezes') do |num_parcelas|
    valor_entrada = $valor_simulacao_proposta[0]
    parcelas = num_parcelas.to_i
    valor_parcela = valor_entrada
    $inclui_acordo = $inclusao_acordo.inclui_acordo($token, $contrato, valor_entrada, $data, parcelas, valor_parcela)
end

Quando('a requisição de simulação de proposta for realizada a prazo com valor de entrada e da parcela nulos') do
    parcelas = 4
    contrato = $dados_contrato[2]
    valor_entrada = 0
    valor_parcela = 0
    $simula_proposta = $simulacao_proposta.simula_proposta($token, contrato, valor_entrada, $data, parcelas, valor_parcela)
end

Quando('a requisição de simulação de proposta for realizada a prazo com valor de entrada nula') do
    parcelas = 4
    valor_divida = $dados_contrato[0]
    contrato = $dados_contrato[2]
    valor_entrada = 0
    valor_parcela = (valor_divida*0.8/parcelas).to_f
    $simula_proposta = $simulacao_proposta.simula_proposta($token, contrato, valor_entrada, $data, parcelas, valor_parcela)
end

Quando('a requisição de simulação de proposta for realizada a prazo com o valor da parcela nula') do
    parcelas = 4
    valor_divida = $dados_contrato[0]
    contrato = $dados_contrato[2]
    valor_entrada = (valor_divida*0.8/parcelas).to_f
    valor_parcela = 0
    $simula_proposta = $simulacao_proposta.simula_proposta($token, contrato, valor_entrada, $data, parcelas, valor_parcela)
end

Quando('a requisição de simulação de proposta for realizada a prazo com um valor vazio') do
    parcelas = 4
    contrato = $dados_contrato[2]
    valor_entrada = ""
    valor_parcela = ""
    $simula_proposta = $simulacao_proposta.simula_proposta($token, contrato, valor_entrada, $data, parcelas, valor_parcela)
end

Quando('a requisição de simulação de proposta for realizada a prazo com um valor string') do
    parcelas = 4
    contrato = $dados_contrato[2]
    valor_entrada = "jdjhdh"
    valor_parcela = "jdjhdh"
    $simula_proposta = $simulacao_proposta.simula_proposta($token, contrato, valor_entrada, $data, parcelas, valor_parcela)
end

Quando('a requisição de simulação de proposta for realizada a prazo com a data de pagamento nula') do
    parcelas = 4
    data = 0
    valor_divida = $dados_contrato[0]
    contrato = $dados_contrato[2]
    valor_entrada = (valor_divida*0.8/parcelas).to_f
    valor_parcela = valor_entrada
    $simula_proposta = $simulacao_proposta.simula_proposta($token, contrato, valor_entrada, data, parcelas, valor_parcela)
end

Quando('a requisição de simulação de proposta for realizada a prazo com a data de pagamento vazia') do
    parcelas = 4
    data = ""
    valor_divida = $dados_contrato[0]
    contrato = $dados_contrato[2]
    valor_entrada = (valor_divida*0.8/parcelas).to_f
    valor_parcela = valor_entrada
    $simula_proposta = $simulacao_proposta.simula_proposta($token, contrato, valor_entrada, data, parcelas, valor_parcela)
end

Quando('a requisição de simulação de proposta for realizada a prazo com a data de pagamento com um valor sring') do
    parcelas = 4
    data = "jfhjfjh"
    valor_divida = $dados_contrato[0]
    contrato = $dados_contrato[2]
    valor_entrada = (valor_divida*0.8/parcelas).to_f
    valor_parcela = valor_entrada
    $simula_proposta = $simulacao_proposta.simula_proposta($token, contrato, valor_entrada, data, parcelas, valor_parcela)
end

Quando('a requisição de inclusão de proposta for realizada a prazo com valor de entrada e da parcela nulos') do
    valor_entrada = 0
    parcelas = 4
    valor_parcela = 0
    $inclui_acordo = $inclusao_acordo.inclui_acordo($token, $contrato, valor_entrada, $data, parcelas, valor_parcela)
end

Quando('a requisição de inclusão de proposta for realizada a prazo com o valor de entrada nula') do
    valor_entrada = 0
    parcelas = 4
    valor_parcela = $valor_simulacao_proposta[0]
    $inclui_acordo = $inclusao_acordo.inclui_acordo($token, $contrato, valor_entrada, $data, parcelas, valor_parcela)
end

Quando('a requisição de inclusão de proposta for realizada a prazo com o valor da parcela nula') do
    valor_entrada = $valor_simulacao_proposta[0]
    parcelas = 4
    valor_parcela = 0
    $inclui_acordo = $inclusao_acordo.inclui_acordo($token, $contrato, valor_entrada, $data, parcelas, valor_parcela)
end

Quando('a requisição de inclusão de proposta for realizada a prazo com um valor vazio') do
    valor_entrada = ""
    parcelas = 4
    valor_parcela = ""
    $inclui_acordo = $inclusao_acordo.inclui_acordo($token, $contrato, valor_entrada, $data, parcelas, valor_parcela)
end

Quando('a requisição de inclusão de proposta for realizada a prazo com um valor string') do
    valor_entrada = "fhhrfg"
    parcelas = 4
    valor_parcela = "fhhrfg"
    $inclui_acordo = $inclusao_acordo.inclui_acordo($token, $contrato, valor_entrada, $data, parcelas, valor_parcela)
end

Quando('a requisição de inclusão de proposta for realizada a prazo com a data de pagamento nula') do
    data = 0
    valor_entrada = $valor_simulacao_proposta[0]
    parcelas = 4
    valor_parcela = valor_entrada
    $inclui_acordo = $inclusao_acordo.inclui_acordo($token, $contrato, valor_entrada, data, parcelas, valor_parcela)
end

Quando('a requisição de inclusão de proposta for realizada a prazo com a data de pagamento vazia') do
    data = ""
    valor_entrada = $valor_simulacao_proposta[0]
    parcelas = 4
    valor_parcela = valor_entrada
    $inclui_acordo = $inclusao_acordo.inclui_acordo($token, $contrato, valor_entrada, data, parcelas, valor_parcela)
end

Quando('a requisição de inclusão de proposta for realizada a prazo com a data de pagamento com um valor sring') do
    data = "jdbfdhf"
    valor_entrada = $valor_simulacao_proposta[0]
    parcelas = 4
    valor_parcela = valor_entrada
    $inclui_acordo = $inclusao_acordo.inclui_acordo($token, $contrato, valor_entrada, data, parcelas, valor_parcela)
end