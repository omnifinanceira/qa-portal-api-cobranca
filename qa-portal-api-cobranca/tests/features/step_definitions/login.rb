Quando('eu preencher o campo de usuário {string}') do |usuario|
    @aut = AutenticacaoPortal.new
    @usuario = usuario
end

Quando('o campo de senha {string}') do |senha|
    @senha = senha
end

Entao('fazer a requisição') do
    @response_login = @aut.fazer_autenticacao(@usuario, @senha)
    $token =  @aut.retorna_token
end

Entao('devo verificar se o status obtido foi {string}') do |status|
    status_code = (@response_login.code).to_s
    expect(status_code).to eq(status)
end

Entao('devo verificar se o status obtido foi {string} com a descrição {string}') do |status, descricao|
    status_code = (@response_login.code).to_s
    expect(status_code).to eq(status)
    expect(@response_login['error_description']).to eq(descricao)
end